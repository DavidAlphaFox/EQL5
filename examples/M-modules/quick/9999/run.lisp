(in-package :eql-user)

(si::trap-fpe t nil)

(qrequire :quick)

(load "lisp/qml-lisp")
(load "lisp/main")

(use-package :qml)

(progn
  (ini-quick-view "qml/main.qml")
  (|setPosition| *quick-view* '(50 50)))

